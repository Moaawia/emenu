import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { DataService } from '../data.service';
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit  {
  userId: any;
  userData: any;
  constructor(private route: ActivatedRoute, private dataService: DataService, private router: Router ) { }
  ngOnInit(): void {
    // Retrieve the 'id' parameter from the route
    this.route.params.subscribe(params => {
      this.userId = +params['id']; 
      this.dataService.getUsers(1).subscribe(response => {
        const users = response.data; // Assuming 'data' property contains user array
        this.userData = users.find((user:any) => user.id === this.userId);
      });
    });
  }
  cancel() {
    // Assuming 'home' is the route name for your home page
    this.router.navigate(['/']);
  }
  
}
