import { Component } from '@angular/core';
import { LoadingService } from './loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'emenutest';
  constructor(public loadingService: LoadingService) {}
  isLoading = false;

  // Example: Show loading indicator before making a network request
  makeNetworkRequest() {
    this.isLoading = true;

    // Simulate a network request
    setTimeout(() => {
      this.isLoading = false;
      // Continue with your application logic
    }, 3000); // Replace with the actual request code
  }
}
