// import { Component, ViewChild } from '@angular/core';
import { DataService } from '../data.service';
// import {MatSort, MatSortModule} from '@angular/material/sort';
// import {MatTableDataSource, MatTableModule} from '@angular/material/table';
// import {MatPaginator, MatPaginatorModule} from '@angular/material/paginator';
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { MatPaginator, MatPaginatorModule } from '@angular/material/paginator';
import { MatSort, MatSortModule } from '@angular/material/sort';
import { MatTableDataSource, MatTableModule } from '@angular/material/table';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';

export interface UserData {
  id: string;
  first_name: string;
  last_name: string;
  email: String;
  avatar: string;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  // standalone: true,
  // imports: [MatFormFieldModule, MatInputModule, MatTableModule, MatSortModule, MatPaginatorModule],
})
export class HomeComponent  {
  displayedColumns: string[] = ['id', 'first_name', 'last_name','actions'];
  dataSource: MatTableDataSource<UserData> = new MatTableDataSource<UserData>([]);
  @ViewChild(MatPaginator) paginator!: MatPaginator ;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private dataService: DataService ) {}
  ngOnInit() {
    this.dataService.getUsers(1).subscribe((response) => {
      const data = response.data;
      this.dataSource = new MatTableDataSource(data);
      console.log(this.dataSource);
      this.dataSource!.paginator = this.paginator;
    this.dataSource!.sort = this.sort;
    console.log('Data Source Length:', this.dataSource.data.length);

    });
  }
  // ngAfterViewInit() {
  //   console.log(this.paginator);
    
  //   this.dataSource!.paginator = this.paginator;
  //   this.dataSource!.sort = this.sort;
  // }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
