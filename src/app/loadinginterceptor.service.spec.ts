import { TestBed } from '@angular/core/testing';

import { LoadinginterceptorService } from './loadinginterceptor.service';

describe('LoadinginterceptorService', () => {
  let service: LoadinginterceptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoadinginterceptorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
