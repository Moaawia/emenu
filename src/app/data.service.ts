import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private http: HttpClient) {}

  getUsers(page: number): Observable<any> {
    const url = `https://reqres.in/api/users?page=${page}`;
    return this.http.get(url);
  }
}
